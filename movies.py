import requests
import pandas as pd
from bs4 import BeautifulSoup


year = input("Enter Year: ")
URL = f"https://www.boxofficemojo.com/year/world/{year}"
response = requests.get(URL)
page = BeautifulSoup(response.content, 'html.parser')


base_url = 'https://www.boxofficemojo.com/'
movie_td = page.find_all(class_='mojo-field-type-release_group')
rank  = [rank.text for rank in page.find_all(class_='mojo-field-type-rank')]
movie_name  = [movie.text.strip() for movie in page.find_all(class_='mojo-field-type-release_group')]
url = [ base_url+td.findChildren('a')[0]['href'] for td in movie_td[1:]]



movies = {rank[0]:rank[1:], movie_name[0]:movie_name[1:], 'Url':url}
movies_df = pd.DataFrame(movies)


movies_df.to_csv(f'{year}-movies.csv', index=False)




